/*
Navicat MySQL Data Transfer

Source Server         : surat
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : db_perpus

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-11-13 11:40:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_buku
-- ----------------------------
DROP TABLE IF EXISTS `tbl_buku`;
CREATE TABLE `tbl_buku` (
  `kode_buku` varchar(100) NOT NULL,
  `judul_buku` varchar(255) NOT NULL,
  `pengarang` varchar(100) NOT NULL,
  `penerbit` varchar(255) CHARACTER SET latin5 NOT NULL,
  `tahun_terbit` text NOT NULL,
  `lokasi_file` text NOT NULL,
  PRIMARY KEY (`kode_buku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_buku
-- ----------------------------
INSERT INTO `tbl_buku` VALUES ('A001', 'Kerdokteran', 'Siapa', 'Gramedia', '2013', 'Yuhuu.pdf');
INSERT INTO `tbl_buku` VALUES ('A002', 'qd', 'aa', 'Erlangga', '2011', 'ser.pdf');
INSERT INTO `tbl_buku` VALUES ('A003', 'Penyakit', 'aa', 'Erlangga', '2012', 'Individual.pdf');

-- ----------------------------
-- Table structure for tbl_counter
-- ----------------------------
DROP TABLE IF EXISTS `tbl_counter`;
CREATE TABLE `tbl_counter` (
  `counts` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_counter
-- ----------------------------
INSERT INTO `tbl_counter` VALUES ('22');

-- ----------------------------
-- Table structure for tbl_gambar
-- ----------------------------
DROP TABLE IF EXISTS `tbl_gambar`;
CREATE TABLE `tbl_gambar` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(100) NOT NULL DEFAULT '',
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_gambar
-- ----------------------------
INSERT INTO `tbl_gambar` VALUES ('58', 'gambar satu', 'a.jpg', 'fe.pdf');
INSERT INTO `tbl_gambar` VALUES ('59', 'gambar dua', 'design fix-01 riot.jpg', 'xaxa.pdf');
INSERT INTO `tbl_gambar` VALUES ('61', 'gambar tiga', '', '1094241PB.pdf');

-- ----------------------------
-- Table structure for tbl_peraturan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_peraturan`;
CREATE TABLE `tbl_peraturan` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `judul_peraturan` varchar(255) NOT NULL,
  `lokasi_file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_peraturan
-- ----------------------------
INSERT INTO `tbl_peraturan` VALUES ('7', 'Peraturan satu', 'Individual.pdf');

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('0', 'user', '122');
INSERT INTO `tbl_user` VALUES ('1', 'admin', '123');
