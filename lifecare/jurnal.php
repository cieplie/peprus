<!DOCTYPE html>
<?php
$cari = '';
if (isset($_GET['cari'])){
  $cari=$_GET['cari'];
} 
?>
<html>
<head>
	 <!-- Basic -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <!-- Mobile Metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1">
   <!-- Site Metas -->
   <title>Perpustakaan</title>
   <meta name="keywords" content="">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- Site Icons -->
   <link rel="shortcut icon" href="images/icon-rspw.png" type="image/x-icon" />
   <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <!-- Site CSS -->
   <link rel="stylesheet" href="style.css">
   <!-- Colors CSS -->
   <link rel="stylesheet" href="css/colors.css">
   <!-- ALL VERSION CSS -->
   <link rel="stylesheet" href="css/versions.css">
   <!-- Responsive CSS -->
   <link rel="stylesheet" href="css/responsive.css">
   <!-- Custom CSS -->
   <link rel="stylesheet" href="css/custom.css">
   <!-- Modernizer for Portfolio -->
   <script src="js/modernizer.js"></script>
   <!-- [if lt IE 9] --></head>
<body>
<!-- <div id="kedokteran" class="parallax section db" data-stellar-background-ratio="0.4" style="background:#fff;" data-scroll-id="kedokteran" tabindex="-1"> -->
    <div class="container">
      
      <div class="heading">
            <span class="icon-logo">
              <img src="images/icon-logo.png" alt="#"></span>
              <h2>This is a Journal Collection</h2>
      </div>

            <?php 
             include "../conf/koneksi.php";

              $batas   = 3;
              $halaman = @$_GET['halaman'];
              if(empty($halaman)){
                 $posisi  = 0;
                 $halaman = 1;
               }
                 else{ 
                   $posisi  = ($halaman-1) * $batas; 
                 }

             $query  = "SELECT * FROM tbl_buku WHERE judul_buku LIKE '%".$cari."%' OR lokasi_file LIKE '%".$cari."%' LIMIT $posisi,$batas";
             $tampil = mysqli_query($con, $query);
            

              $query2     = mysqli_query($con, "select * from tbl_buku WHERE judul_buku LIKE '%".$cari."%' OR lokasi_file LIKE '%".$cari."%'");
              $jmldata    = mysqli_num_rows($query2);
              $jmlhalaman = ceil($jmldata/$batas);

            echo "<br> Halaman : ";

            for($i=1;$i<=$jmlhalaman;$i++)
            if ($i != $halaman){
             echo " 
             <a class='btn btn-primary' data-scroll href=\"jurnal.php?halaman=$i\">$i</a>| ";
            }
            else{ 
             echo " <b>$i</b> | "; 
            }
            echo "<p>Total buku: <b>$jmldata</b> orang</p>";

            while ($perpdf=mysqli_fetch_array($tampil)){
              $nama = str_replace(" ", "%20", $perpdf['lokasi_file']);
            ?>
             
            <div class="row dev-list text-center">
                <div class="col-lg-2.5 col-md-2.5 col-sm-3 col-xs-3 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;">

                  <div class="widget clearfix">
                    <img src="../img/jurnal.jpg" alt="" class="img-responsive img-rounded" width="200px">
                      <div class="widget-title">
                        
                        <a href=<?php echo '../buku/pdf.php?pdf_file='. $nama ?>> <button type='button' class='btn btn-info'> Lihat File PDF</button></a>
                            <?php 
                                echo $perpdf["lokasi_file"];
                             ?>
                        </div>
                        <!-- end title -->
                        <p>
                             <p>
                                 Hello guys,this is a collection of Jurnal.
                             </p>
                          
                       
                       <!--  <div class="footer-social">
                            <a href="#" class="btn grd1"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="btn grd1"><i class="fa fa-github"></i></a>
                            <a href="#" class="btn grd1"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="btn grd1"><i class="fa fa-linkedin"></i></a>
                        </div> -->
                    </div><!--widget -->
                </div>
<?php } ?>
</body>
</html>