<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('assets/tcpdf/tcpdf.php');
include 'konek.php';
include 'get.php';
$no_urut 	= $_GET['no_urut'];
$data 		= mysqli_query($konek,"SELECT * FROM tb_surat WHERE no_urut = '$no_urut'");
$result =mysqli_fetch_array($data);

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('No Surat Kematian An. '.$result['nm_pasien'].'');
$pdf->SetAutoPageBreak(true);
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

include 'konek.php';
$no_urut 	= $_GET['no_urut'];
$data 		= mysqli_query($konek,"SELECT * FROM tb_surat WHERE no_urut = '$no_urut'");
while ($result 	= mysqli_fetch_array($data)) {


// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// Set some content to print
$html='
<table>
	<tr>
		<th width="30%"><h4>Surat Kematian</h4></th>
		<th width="30%"></th>
		<th width="40%">No Surat : '.$result['no_surat'].'</th>
	</tr>
</table><br>
<div style="background-color:#000; color:white; padding: 1,1,1,1;"></div><br>
<table>
	<tr>
		<th width="40%">Yang bertanda tangan dibawah ini</th>';
$html.='<th width="60%">:</th>
	</tr>
	<tr>
		<th width="40%">Menerangkan bahwa pada tanggal</th>
		<th width="60%">: </th>
	</tr>
</table>
<br><br>
Mengaku dengan mengingat sumpah telah memeriksa jenazah :<br><br>';
$html.= '<table>
		<tr>
			<th width="25%">No. RM</th>
			<th width="75%">: <span style="text-transform: capitalize;">'.$result['no_rm'].'</span></th>
		</tr>
		<tr>
			<th width="25%">Nama</th>
			<th width="75%">: <span style="text-transform: capitalize;">'.$result['nm_pasien'].'</span></th>
		</tr>
		<tr>
			<th width="25%">Jenis Kelamin</th>
			<th width="75%">: '.$result['jk'].'</th>
		</tr>
		<tr>
			<th width="25%">Tempat & Tgl Lahir</th>
			<th width="75%">: '.$result['tmpt_lahir'].', '.tgl_indo($result['tgl_lahir']).' </th>
		</tr>
		<tr>
			<th width="25%">Alamat</th>
			<th width="75%">: '.$result['alamat'].'</th>
		</tr>
		<tr>
			<th width="25%">Tgl Meninggal</th>
			<th width="75%">: '.tgl_indo($result['tgl_meninggal']).'</th>
		</tr>
		<tr>
			<th width="25%">Jam Meninggal</th>
			<th width="75%">: '.$result['jam_meninggal'].' WIB</th>
		</tr>
	</table>
	<br><br>
	Demikian surat kematian ini kami buat, agar dapat dipergunakan sebagaimana mestinya.
	<br><br><br>
	<table>
		<tr>
			<th width="40%"></th>
			<th width="25%"></th>';
$html.='<th width="35%">Malang, <br>Dokter yang memeriksa<br><br><br><br><br><span style="font-style:italic; text-decoration: overline;">Tanda tangan & Nama Lengkap</span></th>
		</tr>
	</table><br>
	<div style="background-color:#000; color:white; padding: 1px,1px,1px,1px">
</div>';
}

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// $pdf->SetLineWidth(2);



// set alpha to semi-transparency
$pdf->SetAlpha(0.1);
$pdf->SetFont('helvetica', 'B', 120, '', true);
$pdf->StartTransform();
//text asli
$pdf->SetFillColor(255);
$pdf->SetDrawColor(255);
$pdf->Rotate(45, 70, 125);
$pdf->Text(37, 120, 'ASLI');

$pdf->StopTransform();

// restore full opacity
// $pdf->SetAlpha(1);
// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+