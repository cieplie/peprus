<html>
<head>
	<title>Edit Data Buku</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script scr="../jquery.js"></script>
       <script scr="../js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Form Data Buku</h2>
  <form action="../buku/update.php" method="POST">

    <?php
      include"../conf/koneksi.php";

     if(isset($_GET['kode'])){
      $sql=mysqli_query($con, "select * from tbl_buku where kode_buku='$_GET[kode]'");

      $r =mysqli_fetch_array($sql);

      } 
    ?>
    
    <div class="form-group">
      <label for="kode">Kode Buku</label>
      <input type="kode" class="form-control" id="kode" placeholder="Masukan Kode Buku"value ="<?php echo $r['kode_buku']; ?>" name="kode_buku" readonly>
    </div>

    <div class="form-group">
      <label for="judul">Judul Buku</label>
      <input type="judul" class="form-control" id="judul" placeholder="Masukkan Judul Buku" name="judul_buku" value=<?php echo $r['judul_buku'] ?>>
    </div>

	<div class="form-group">
      <label for="pengarang">Pengarang</label>
      <input type="pengarang" class="form-control" id="pengarang" placeholder="Masukkan Pengarang Buku" name="pengarang" value =<?php echo $r['pengarang'];  ?>>
    </div>
    <div class="form-group">
      <label for="penerbit">Penerbit</label>
      <input type="penerbit" class="form-control" id="penerbit" placeholder="Masukkan Penerbit Buku" name="penerbit" value =<?php echo $r['penerbit']; ?>>
    </div>
     <div class="form-group">
      <label for="sel1">Tahun Terbit Buku:</label>
      <br><select name="tahun_terbit" value=<?php echo $r['tahun_terbit']; ?>>
                <option value="">Please Select</option>
                <?php
                $thn_skr = date('Y');
                for ($x = $thn_skr; $x >= 2010; $x--) {
                ?>
                    <option value="<?php echo $x ?>"><?php echo $x ?></option>
                <?php
                }
                ?>
            </select><br>


					<div class="form-group">
          <label for="Upload">Upload File FDF:</label>
          <div class="value">
          <div class="input-group js-input-file">
          <input class="input-file" type="file" name="lokasi_file" id="lokasi_file">
                                    <!-- <label class="label--file" for="file">Choose file</label>
                                    <span class="input-file__info">No file chosen</span> -->
          </div>
           <div class="label--desc">Upload your CV/Resume or any other relevant file. Max file size 50 MB</div>
          </div>
          </div>


         <p>
      <input type="submit" value="Simpan" name="submit">    
    </p>
    
  </form>
</div>

<?php
{}
?>


</body>
</html>