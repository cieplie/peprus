<html>
<head>
	<title>Tampil Buku</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script scr="../jquery.js"></script>
    <script scr="../js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Form Data Buku</h2>
 <form action="../buku/simpan.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <label for="kode">Kode Buku</label>
      <input type="kode" class="form-control" id="kode" placeholder="Masukan Kode Buku" name="kode_buku">
    </div>

    <div class="form-group">
      <label for="judul">Judul Buku</label>
      <input type="judul" class="form-control" id="judul" placeholder="Masukkan Judul Buku" name="judul_buku">
    </div>
    
	<div class="form-group">
      <label for="pengarang">Pengarang</label>
      <input type="pengarang" class="form-control" id="pengarang" placeholder="Masukkan Pengarang Buku" name="pengarang">
    </div>
    <div class="form-group">
      <label for="penerbit">Penerbit</label>
      <input type="penerbit" class="form-control" id="penerbit" placeholder="Masukkan Penerbit Buku" name="penerbit">
    </div>
     <div class="form-group">
      <label for="sel1">Tahun Terbit Buku:</label>
      <br><select name="tahun_terbit">
                <option value="">Please Select</option>
                <?php
                $thn_skr = date('Y');
                for ($x = $thn_skr; $x >= 2010; $x--) {
                ?>
                    <option value="<?php echo $x ?>"><?php echo $x ?></option>
                <?php
                }
                ?>
            </select><br>

      
					<!-- <div class="form-group">
                            <label for="Upload">Upload File:</label>
                            <div class="value">
                                <div class="input-group js-input-file">
                                    <input class="input-file" type="file" name="lokasi_file" id="lokasi_file">
                                    
                                </div>
                                <div class="label--desc">Upload your CV/Resume or any other relevant file. Max file size 50 MB</div>
                            </div>
                        </div> -->
       <div class="form-group">
          <label for="pdf">Upload File FDF</label> Upload your CV/Resume or any other relevant file. Max file size 1000KB
          <br><input type="file" name="fileToUpload" id="fileToUpload">
      </div>
      
    <p>
      <input type="submit" value="Simpan" name="submit">    
    </p>
                  

    
  </form>
</div>




</body>
</html>