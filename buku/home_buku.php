<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>Tampil Barang</title>
  <link rel="stylesheet" href="../css/bootstrap.min.css" />
  <link rel="stylesheet" href="../DataTables/datatables.min.csss" />
  <link rel="stylesheet" type="text/css" href="../fontawesome/css/all.css" />
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/jquery.js"></script>
  
  <script src="../DataTables/datatables.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../DataTables/DataTabless/css/jquery.dataTables.min.css" />
  <link rel="stylesheet" type="text/css" href="../DataTables/responsive.css" />
  <script src="../DataTables/DataTabless/js/jquery.dataTables.min.js"></script>
  <script src="../DataTables/responsive.js"></script>
  <script>

    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
  </script>
  <style>
  .fa-plus-circle { 
  color: #33a008; 
  }
  .fa-trash-alt { 
  color: #b51515; 
  }
  .fa-print { 
  color: #bb7c18; 
  }
  
  
  </style>
</head>

<body>
  <?php
  include "../conf/koneksi.php";
  $tampil = mysqli_query($con, "SELECT * FROM tbl_buku");
  ?>
  <div class="container">
    <h2>Input File Jurnal</h2>
        <!-- <a href="add.php"><i class="fas fa-plus-circle"></i></a> -->
        
        <p>
        
        <a href="../buku/add.php"> <button type='button' class='btn btn-info' >Tambah Buku</button></a>
        
        </p>

    </p>

    <table id="myTable" class="display responsive nowrap" cellspacing="0" width="100%">

      <thead>
        <tr>
          <th>No</th>
          <th>Kode Buku</th>
          <th>Judul Buku</th>
          <th>Pengarang</th>
          <th>Penerbit</th>
          <th>Tahun Terbit</th>
          <th>PDF</th>
          <th>Aksi</th>
          
        </tr>
      </thead>
      <tbody>

        <?php
        $no = 1;
        while ($r = mysqli_fetch_array($tampil)) {
          $nama = str_replace(" ", "%20", $r['lokasi_file']);
          echo "<tr>
          <td>$no</td>
          <td>$r[kode_buku]</td>
          <td>$r[judul_buku]</td>
          <td>$r[pengarang]</td>
          <td>$r[penerbit]</td>
          <td>$r[tahun_terbit]</td>
          <td>$r[lokasi_file]</td>
          <td>";

          // $teq = $r[lokasi_file];
          ?>
          
          <a href=<?php echo '../buku/pdf.php?pdf_file='. $nama; ?>> 
          <button type='button' class='btn btn-info'>File PDF</button></a>


        
       <a href=<?php echo '../buku/edit.php?kode='. $r["kode_buku"] ?> >
		 		<button type='button' class='btn btn-primary'>Edit</button>
		 		</a>

		 		<a href=<?php echo '../buku/delete.php?kode='. $r["kode_buku"] ?> >
				<button type='button' class='btn btn-danger'>Hapus</button>
				</a>
          </td>
          </tr>

          <?php
          $no++;
        } ?>
      </tbody>
    </table>
  </div>
</body>

</html>