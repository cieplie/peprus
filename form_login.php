<!DOCTYPE html>
<html>
<head>
    <title>Form Login </title>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="login/style.css">
    </head>

<body>
    <div class="d-flex justify-content-center h-100">
        <div class="user_card">
        <div class="d-flex justify-content-center">
        <div class="brand_logo_container">
        <img src="img/logo.png" class="brand_logo" alt="Logo">
        </div>
        </div>
        <div class="d-flex justify-content-center form_container">
    <!-- cek pesan notifikasi -->
    <?php 
    if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "gagal"){
            echo "Login gagal! username dan password salah!";
        }else if($_GET['pesan'] == "logout"){
            echo "Anda telah berhasil logout";
        }else if($_GET['pesan'] == "belum_login"){
            echo "Anda harus login untuk mengakses halaman admin";
        }
    }
    ?>
    <div class="container" align="center">
    <div align="center" style="width:250px;margin-top:5%;">

    <form method="post" action="cek_login.php">
        <div class="input-group mb-3">
        <div class="input-group-append">
        <span class="input-group-text"><i class="fas fa-user"></i></span>
        </div>
        <input type="text" name="username" class="form-control input_user" value="" placeholder="username">
        </div>


        <div class="input-group mb-2">
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" name="password" class="form-control input_pass" value="" placeholder="password">
        </div>

        <div class="form-group">
            <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="customControlInline">
            <label class="custom-control-label" for="customControlInline">Remember me</label>
            </div>
        </div>


        <div class="d-flex justify-content-center mt-3 login_container">
        <button type="submit" name="button" class="btn login_btn">Login</button>
        </div>
     
      <!-- <a href="mhs/mhs_home.php" type="button" class="btn btn-block btn-info">Register Admin</a> -->
      <!-- <button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-eye-open"></span> Lupa Password ?</button> -->  
    </form> 
    <div class="mt-4">
        <div class="d-flex justify-content-center links">Don't have an account?
        <a href="#" class="ml-2">Sign Up</a>
        </div>
        <div class="d-flex justify-content-center links">
        <a href="#">Forgot your password?</a>
        </div>
    </div>

</body>
</html>